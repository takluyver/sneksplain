import site
import sys
from pathlib import Path

def is_venv(dir: Path):
    return (dir / 'pyvenv.cfg').is_file()

def describe():
    print(f"Interpreter: {sys.executable}")
    print(f"Python version: {sys.version}")
    env_dir = Path(sys.executable).parents[1]
    if (env_dir / 'pyvenv.cfg').is_file():
        base_exe = Path(sys.executable).resolve()
        print(f"Virtualenv based on: {base_exe}")
    elif (env_dir / 'conda-meta').is_dir():
        print("Conda environment")

    print(f"User site-packages: {site.ENABLE_USER_SITE}")
    print("Imports packages from:")
    for d in sys.path:
        print(f"- {d}")

if __name__ == '__main__':
    describe()
