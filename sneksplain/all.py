from functools import lru_cache
import json
import os
import re
from pathlib import Path
from subprocess import run, PIPE

if os.name == 'nt':
    PYTHON_PAT = re.compile(r'python[\d.]*\.exe$')
    ENV_GLOB = "Scripts\\python.exe"
else:
    PYTHON_PAT = re.compile(r'python[\d.]*$')
    ENV_GLOB = "bin/python"

DUMP_INFO = """
import json, sys
print(json.dumps({"version": sys.version}))
"""

@lru_cache()
def get_interpreter_info(exe_path):
    r = run([str(exe_path), '-c', DUMP_INFO], stdout=PIPE)
    if r.returncode != 0:
        return {'version': '???', 'version_num': '???'}
    else:
        d = json.loads(r.stdout)
        d['version_num'] = d['version'].split(' ')[0]
        return d

def pythons_on_path():
    pathdirs = (os.environ.get('PATH', '') or os.defpath).split(os.pathsep)
    for pathdir in pathdirs:
        pathdir = Path(pathdir).expanduser()
        if not pathdir.is_dir():
            continue

        for p in sorted(pathdir.iterdir()):
            if not (PYTHON_PAT.match(p.name) and p.is_file()):
                continue

            iinf = get_interpreter_info(p.resolve())
            version = iinf['version_num']
            if not p.is_symlink():
                print(f"{p} ({version})")
            else:
                tgt = (p.parent / p.readlink())
                if tgt.parent == p.parent:
                    print(f"{p} -> {tgt.name}")
                elif (pathdir.parent / 'pyvenv.cfg').is_file():
                    print(f"{p} : venv based on {tgt} ({version})")
                else:
                    print(f"{p} -> {tgt} ({version})")


def find_envs():
    print("Searching for environments...")
    for python in Path.home().rglob(ENV_GLOB):
        env = python.parents[1]
        version = get_interpreter_info(python.resolve())['version_num']
        if (env / 'pyvenv.cfg').is_file():
            env_type = 'virtualenv, '
        elif (env / 'conda-meta').is_dir():
            env_type = 'conda env, '
        else:
            env_type = ''
        print(f"{env} ({env_type}{version})")

if __name__ == '__main__':
    pythons_on_path()
    print()
    find_envs()
